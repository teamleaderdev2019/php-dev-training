<?php
include "config.php";

include "header.php";
include "navigation.php";
include "sidebar.php";

$sqlQuery = "SELECT * FROM student_record";
$result = mysqli_query($conn, $sqlQuery);

?>
<div>
    <?php
    if (isset($_GET['msg'])){
        echo $_GET['msg'];
    }
    ?>
</div>
<main>
    <a href="add.php">Add</a>
    <table border="1" style="border: 1px solid black; border-collapse: collapse;">
        <tr>
            <td>Student Id</td>
            <td>Firstname</td>
            <td>Lastname</td>
            <td>Course</td>
            <td>Age</td>
            <td>Hobby</td>
            <td>Action</td>
        </tr>

        <?php
        while($row = mysqli_fetch_assoc($result)) {
            ?>
            <tr>
                <td><?php echo $row['student_id']; ?></td>
                <td><?php echo $row['firstname']; ?></td>
                <td><?php echo $row['lastname']; ?></td>
                <td><?php echo $row['course']; ?></td>
                <td><?php echo $row['age']; ?></td>
                <td><?php echo $row['hobby']; ?></td>
                <td><a href="delete.php?student_id=<?php echo $row['student_id']; ?>">Delete</a> <button>Edit</button></td>
            </tr>

        <?php
        }
        ?>
    </table>

</main>

<?php
include "footer.php";

?>
