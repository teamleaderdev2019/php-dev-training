<?php

include "header.php";
include "navigation.php";
include "sidebar.php";

?>

<main>
   <a href="records.php">Back</a>
    <form action="save.php" method="post">
    <table border="1" style="border: 1px solid black; border-collapse: collapse;">

        <tr>
            <td>Firstname</td>
            <td><input type="text" name="firstname"> </td>
        </tr>

        <tr>
            <td>Lastname</td>
            <td><input type="text" name="lastname"> </td>
        </tr>

        <tr>
            <td>Course</td>
            <td><input type="text" name="course"> </td>
        </tr>

        <tr>
            <td>Age</td>
            <td><input type="number" name="age"> </td>
        </tr>
        <tr>
            <td>Hobby</td>
            <td><input type="text" name="hobby"> </td>
        </tr>

        <tr>
            <td colspan="2"><button type="submit" >Save</button></td>
        </tr>
    </table>
    </form>

</main>

<?php
include "footer.php";

?>
